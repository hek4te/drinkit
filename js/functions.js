(function() {
  'use strict';
  const waterRequirement = 3.5;

  var drinkRecords = {
    records : [],
    getrecords : function(){
      this.records = JSON.parse(localStorage.getItem('records')) || [];
      return this.records;
    },
    updateRecords : function(waterListRecord){
      this.records.push(waterListRecord);
    },
  };

  var lastdrinkedrecord = {
    lastDrinked:'',
    getlastdrinked : function(){
      if(localStorage.getItem('lastDrinked')){
        this.lastDrinked = new Date(localStorage.getItem('lastDrinked'));
      }else {
        this.lastDrinked = new Date();
      }
    },
    updatelastdrinked : function(dateString){
      this.lastDrinked = dateString;
    }
  };

  function runTimer(){
    let timer = document.querySelector('.timer');
    let lastDrinkDate, currentDate, timeDiffHours, timeDiffMinutes,
    timeMinutesTotalCurrent, timeMinutesTotalLast, timeMinutesTotal;
      setInterval(function(){
        lastDrinkDate = new Date(lastdrinkedrecord.lastDrinked);
        currentDate = new Date();
        timeMinutesTotalCurrent = (currentDate.getHours()*60) + currentDate.getMinutes();
        timeMinutesTotalLast = (lastDrinkDate.getHours()*60) + lastDrinkDate.getMinutes();
        timeMinutesTotal = timeMinutesTotalCurrent - timeMinutesTotalLast;
        timeDiffMinutes = timeMinutesTotal % 60;
        timeDiffHours = Math.floor(timeMinutesTotal / 60);
        timer.innerHTML = timeDiffHours + ' hours ' + timeDiffMinutes + ' minutes';
      },1000);
  }

  function createListRecord(waterListRecord){
    let waterListItem = document.createElement('li');
    let waterListItemText = document.createTextNode(
      waterListRecord.time + ' , drinked:  '+
      waterListRecord.record + 'liters of water.');
    waterListItem.appendChild(waterListItemText);
    return waterListItem;
  }

  function initializeRecords(records){
    if(records){
      records.forEach(function(element){
        let drinkRecordList = document.querySelector('.drinkRecordList');
        drinkRecordList.appendChild(createListRecord(element));
        increeseTotal(element.record);
      });
    }
  }

  function recordDrinkObject(measure){
    let date = new Date();
    let waterListRecord = {};
    waterListRecord.time = date.toString();
    waterListRecord.record = measure;
    lastdrinkedrecord.updatelastdrinked(date.toString());
    drinkRecords.updateRecords(waterListRecord);
    localStorage.setItem('lastDrinked',JSON.stringify(date.toString()));
    localStorage.setItem('records',JSON.stringify(drinkRecords.records));
    return waterListRecord;
  }

  function recordDrink(measure){
    let waterListRecord = recordDrinkObject(measure);
    let drinkRecordList = document.querySelector('.drinkRecordList');
    drinkRecordList.appendChild(createListRecord(waterListRecord));
  }

  function increeseTotal(val){
    let totalValue = parseFloat(document.querySelector('.total').innerHTML);
    let newTotalValue = totalValue + parseFloat(val);
    if(newTotalValue >= waterRequirement)
    document.querySelector('.total').classList.add('green');
    document.querySelector('.total').innerHTML = (newTotalValue).toFixed(1);
  }

  function drinkBtnFunctions(){
    document.querySelector('.iconsWrapper').addEventListener('click',function(){
      let measure = document.querySelector('.measure').value;
      if(measure){
        recordDrink(measure);
        increeseTotal(measure);
      }
    });
  }

  function init(){
    drinkBtnFunctions();
    initializeRecords(drinkRecords.getrecords());
    lastdrinkedrecord.getlastdrinked();
    runTimer();
  }

  init();


/*
TODO:
* reset total Daily
* daily records / records History
* refactor:
- ES6 classes - DrinkRecorder, Timer
- split functions.js
*unit tests
*/


})();
